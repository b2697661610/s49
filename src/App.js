import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Courses from './pages/Courses';

import {Container} from 'react-bootstrap';

import './App.css';

function App() {
  return (
    // <></> fragments - common pattern in React for component to return multiple elements
    <>
      < AppNavbar/>
      <Container>
      < Home/>
      < Courses/>
      </Container>
    </>
  );
