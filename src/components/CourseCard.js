// // [S50 ACTIVITY]
// import { Button, Row, Col, Card } from 'react-bootstrap';
// // [S50 ACTIVITY END]


// // [S50 ACTIVITY]
// export default function CourseCard() {
// return (
//     <Row className="mt-3 mb-3">
//             <Col xs={12}>
//                 <Card className="cardHighlight p-0">
//                     <Card.Body>
//                         <Card.Title>
//                             <h4>Sample Course</h4>
//                         </Card.Title>
//                         <Card.Text>
//                           <p>Description:</p>
//                           <p>This is a sample course offering</p>
//                           <p>Price:</p>
//                           <p>PHP 40,000</p>
//                         </Card.Text>
//                         <Button variant="primary">Enroll</Button>
//                     </Card.Body>
//                 </Card>
//             </Col>
//     </Row>        
// 	)
// }
// // [S50 ACTIVITY END]

// [S50 ACTIVITY]
// import { Button, Row, Col, Card } from 'react-bootstrap';
// // [S50 ACTIVITY END]


// // [S50 ACTIVITY]
// export default function CourseCard({course}) {
// const { name, description, price}= course;


// return (
//     <Row className="mt-3 mb-3">
//             <Col xs={12}>
//                 <Card className="cardHighlight p-0">
//                     <Card.Body>
//                         <Card.Title><h4>{name}</h4></Card.Title>
//                         <Card.Subtitle>Description</Card.Subtitle>
//                         <Card.Text>{description}</Card.Text>
//                         <Card.Subtitle>Price</Card.Subtitle>
//                         <Card.Text>{price}</Card.Text>
//                         <Button variant="primary">Enroll</Button>
//                     </Card.Body>
//                 </Card>
//             </Col>
//     </Row>        
//     )
// }

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard() {
export default function CourseCard({course}) {

    // Deconstruct the course properties into their own variables
    const { name, description, price } = course;

return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>Sample Course</h4></Card.Title>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>This is a sample course offering</Card.Text>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PhP 40000</Card.Text>
                        <Card.Text>{price}</Card.Text>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]

