import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	console.log(coursesData);

	return (
		<>
		{/*way to declare props drilling*/}
		{/*We can pass information from one component to another using props(props drilling)*/}
		{/* Curly braces{} are used for props to signify that we are providing/passing information from one component to another using JS expression*/}
<CourseCard course = {coursesData[0]} />	
		)
	
}